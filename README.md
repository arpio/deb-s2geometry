# S2 Geometry Library

[![pipeline status](https://gitlab.com/arpio/deb-s2geometry/badges/main/pipeline.svg)](https://gitlab.com/arpio/deb-s2geometry/-/commits/main)

## Droidian Packaging

Packaging of [S2 Geometry Library](https://github.com/google/s2geometry) for [Droidian](https://droidian.org)